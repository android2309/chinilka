package diplom_project.chinilka.dto;

import diplom_project.chinilka.models.Category;
import jakarta.persistence.*;
import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeviceTypeDto extends GenericDto{
    private Long categoryId;
    private String name;
    private String description;
    private String image_path;
}
