package diplom_project.chinilka.dto;

import diplom_project.chinilka.models.Role;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends GenericDto{
    private Long roleId;
    private String name;
    private String email;
    private boolean emailConfirmed;
    private String phoneNumber;
    private LocalDateTime dateRegistered;
    private String login;
    private String password;
    private List<Long> orderIds;
}
