package diplom_project.chinilka.dto;

import diplom_project.chinilka.models.Order;
import diplom_project.chinilka.models.Product;
import jakarta.persistence.*;
import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderLineDto extends GenericDto{
    private Long orderId;
    private Long productId;
    private Integer quantity;
}
