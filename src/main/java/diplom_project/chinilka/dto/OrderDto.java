package diplom_project.chinilka.dto;

import diplom_project.chinilka.models.User;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto extends GenericDto{
    private Long userId;
    private LocalDateTime dateCreated;
    private LocalDateTime dateShipped;
    private boolean shipped;
    private List<Long> orderLineIds;
}
