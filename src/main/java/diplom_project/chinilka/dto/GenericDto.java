package diplom_project.chinilka.dto;

import lombok.*;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GenericDto {
    @Nullable
    protected Long id;
    protected String createdBy;
    protected LocalDateTime createdWhen;
    protected String updatedBy;
    protected LocalDateTime updatedWhen;
}
