package diplom_project.chinilka.dto;

import diplom_project.chinilka.models.DeviceType;
import jakarta.persistence.*;
import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto extends GenericDto{
    private Long deviceTypeId;
    private String name;
    private String description;
    private Double price;
    private String imagePath;
}
