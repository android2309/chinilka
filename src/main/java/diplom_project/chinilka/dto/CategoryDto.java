package diplom_project.chinilka.dto;

import diplom_project.chinilka.models.DeviceType;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto extends GenericDto{
    private String name;
    private String description;
    private List<Long> deviceTypeIds;
}
