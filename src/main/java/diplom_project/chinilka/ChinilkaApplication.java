package diplom_project.chinilka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChinilkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChinilkaApplication.class, args);
	}

}
