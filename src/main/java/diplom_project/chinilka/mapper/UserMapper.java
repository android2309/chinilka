package diplom_project.chinilka.mapper;

import diplom_project.chinilka.dto.UserDto;
import diplom_project.chinilka.models.User;
import diplom_project.chinilka.repository.IOrderRepository;
import diplom_project.chinilka.repository.IRoleRepository;
import diplom_project.chinilka.models.GenericModel;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<User, UserDto>{
    private final IRoleRepository roleRepository;
    private final IOrderRepository orderRepository;

    public UserMapper(ModelMapper modelMapper, IRoleRepository roleRepository, IOrderRepository orderRepository) {
        super(User.class, UserDto.class, modelMapper);
        this.roleRepository = roleRepository;
        this.orderRepository = orderRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDto.class)
                .addMappings(m -> m.skip(UserDto::setRoleId)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(UserDto::setOrderIds)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(UserDto.class, User.class)
                .addMappings(m -> m.skip(User::setRole)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setOrders)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(User source, UserDto destination) {
        destination.setRoleId(source.getRole().getId());
        destination.setOrderIds(getIncludeIds(source));
    }

    @Override
    protected void mapSpecificFields(UserDto source, User destination) {
        setRole(source, destination);
        setOrders(source, destination);
    }

    @Override
    protected List<Long> getIncludeIds(User source) {
        return Objects.isNull(source) || Objects.isNull(source.getOrders()) || source.getOrders().isEmpty()
                ? Collections.EMPTY_LIST
                : source.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

    private void setRole(UserDto source, User destination) {
        if (!Objects.isNull(source.getRoleId())) {
            destination.setRole(roleRepository
                    .findById(source.getRoleId())
                    .orElseThrow()
            );
        } else {
            destination.setRole(null);
        }
    }

    private void setOrders(UserDto source, User destination) {
        if (!Objects.isNull(source.getOrderIds())) {
            destination.setOrders(orderRepository.findAllById(source.getOrderIds()));
        } else {
            destination.setOrders(Collections.emptyList());
        }
    }
}
