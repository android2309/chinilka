package diplom_project.chinilka.mapper;

import diplom_project.chinilka.dto.OrderLineDto;
import diplom_project.chinilka.models.OrderLine;
import diplom_project.chinilka.repository.IOrderRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderLineMapper extends GenericMapper<OrderLine, OrderLineDto> {
    private final IOrderRepository orderRepository;

    public OrderLineMapper(ModelMapper mapper, IOrderRepository orderRepository) {
        super(OrderLine.class, OrderLineDto.class, mapper);
        this.orderRepository = orderRepository;
    }

    @Override
    protected void setupMapper() {

    }

    @Override
    protected void mapSpecificFields(OrderLine source, OrderLineDto destination) {

    }

    @Override
    protected void mapSpecificFields(OrderLineDto source, OrderLine destination) {

    }

    @Override
    protected List<Long> getIncludeIds(OrderLine source) {
        return null;
    }
}
