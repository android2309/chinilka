package diplom_project.chinilka.mapper;

import diplom_project.chinilka.dto.RoleDto;
import diplom_project.chinilka.models.Role;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoleMapper extends GenericMapper<Role, RoleDto>{

    public RoleMapper(ModelMapper modelMapper) {
        super(Role.class, RoleDto.class, modelMapper);
    }


    @Override
    protected void setupMapper() {

    }

    @Override
    protected void mapSpecificFields(Role source, RoleDto destination) {

    }

    @Override
    protected void mapSpecificFields(RoleDto source, Role destination) {

    }

    @Override
    protected List<Long> getIncludeIds(Role source) {
        return null;
    }
}
