package diplom_project.chinilka.mapper;

import diplom_project.chinilka.dto.ProductDto;
import diplom_project.chinilka.models.Product;
import diplom_project.chinilka.repository.IDeviceTypeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductMapper extends GenericMapper<Product, ProductDto> {
    private final IDeviceTypeRepository deviceTypeRepository;

    public ProductMapper(ModelMapper modelMapper, IDeviceTypeRepository deviceTypeRepository) {
        super(Product.class, ProductDto.class, modelMapper);
        this.deviceTypeRepository = deviceTypeRepository;
    }

    @Override
    protected void setupMapper() {

    }

    @Override
    protected void mapSpecificFields(Product source, ProductDto destination) {

    }

    @Override
    protected void mapSpecificFields(ProductDto source, Product destination) {

    }

    @Override
    protected List<Long> getIncludeIds(Product source) {
        return null;
    }
}
