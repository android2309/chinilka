package diplom_project.chinilka.mapper;

import diplom_project.chinilka.dto.CategoryDto;
import diplom_project.chinilka.models.Category;
import diplom_project.chinilka.repository.IDeviceTypeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryMapper extends GenericMapper<Category, CategoryDto>{
    private final IDeviceTypeRepository deviceTypeRepository;

    public CategoryMapper(ModelMapper modelMapper, IDeviceTypeRepository deviceTypeRepository) {
        super(Category.class, CategoryDto.class, modelMapper);
        this.deviceTypeRepository = deviceTypeRepository;
    }

    @Override
    protected void setupMapper() {

    }

    @Override
    protected void mapSpecificFields(Category source, CategoryDto destination) {

    }

    @Override
    protected void mapSpecificFields(CategoryDto source, Category destination) {

    }

    @Override
    protected List<Long> getIncludeIds(Category source) {
        return null;
    }
}
