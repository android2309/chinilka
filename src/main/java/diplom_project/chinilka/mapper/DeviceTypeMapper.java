package diplom_project.chinilka.mapper;

import diplom_project.chinilka.dto.CategoryDto;
import diplom_project.chinilka.dto.DeviceTypeDto;
import diplom_project.chinilka.models.Category;
import diplom_project.chinilka.models.DeviceType;
import diplom_project.chinilka.repository.ICategoryRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DeviceTypeMapper extends GenericMapper<DeviceType, DeviceTypeDto>{
    private final ICategoryRepository categoryRepository;

    public DeviceTypeMapper(ModelMapper mapper, ICategoryRepository categoryRepository){
        super(DeviceType.class, DeviceTypeDto.class, mapper);
        this.categoryRepository = categoryRepository;
    }

    @Override
    protected void setupMapper() {

    }

    @Override
    protected void mapSpecificFields(DeviceType source, DeviceTypeDto destination) {

    }

    @Override
    protected void mapSpecificFields(DeviceTypeDto source, DeviceType destination) {

    }

    @Override
    protected List<Long> getIncludeIds(DeviceType source) {
        return null;
    }
}
