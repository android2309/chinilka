package diplom_project.chinilka.mapper;

import diplom_project.chinilka.dto.GenericDto;
import diplom_project.chinilka.models.GenericModel;

import java.util.List;

public interface IMapper<T extends GenericModel, U extends GenericDto> {
    T toEntity(U dto);

    U toDto(T entity);

    List<T> toEntities(List<U> dtoList);

    List<U> toDtos(List<T> entityList);
}
