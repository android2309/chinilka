package diplom_project.chinilka.mapper;

import diplom_project.chinilka.dto.OrderDto;
import diplom_project.chinilka.models.Order;
import diplom_project.chinilka.repository.IOrderLineRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderMapper extends GenericMapper<Order, OrderDto> {
    private final IOrderLineRepository orderLineRepository;

    public OrderMapper(ModelMapper modelMapper, IOrderLineRepository orderLineRepository) {
        super(Order.class, OrderDto.class, modelMapper);
        this.orderLineRepository = orderLineRepository;
    }

    @Override
    protected void setupMapper() {

    }

    @Override
    protected void mapSpecificFields(Order source, OrderDto destination) {

    }

    @Override
    protected void mapSpecificFields(OrderDto source, Order destination) {

    }

    @Override
    protected List<Long> getIncludeIds(Order source) {
        return null;
    }
}
