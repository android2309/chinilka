package diplom_project.chinilka.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "orders", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class Order extends GenericModel {
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_USER_ORDER"))
    private User user;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @JoinTable(name = "order_order_lines",
            joinColumns = @JoinColumn(name = "order_id"), foreignKey = @ForeignKey(name = "FK_ORDER_ORDER_LINES"),
            inverseJoinColumns = @JoinColumn(name = "order_line_id"), inverseForeignKey = @ForeignKey(name = "FK_ORDER_LINE_ORDER")
    )
    private List<OrderLine> orderLines;

    @Column(name = "date_created", nullable = false)
    private LocalDateTime dateCreated;

    @Column(name = "date_shipped")
    private LocalDateTime dateShipped;

    @Column(name = "name", nullable = false)
    private boolean shipped;
}
