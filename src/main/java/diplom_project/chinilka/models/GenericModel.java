package diplom_project.chinilka.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@MappedSuperclass
@Getter
@Setter
@ToString
@NoArgsConstructor
public abstract class GenericModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "created_when")
    private LocalDateTime createdWhen;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "updated_when")
    private LocalDateTime updatedWhen;
    @Column(name = "updated_by")
    private String updatedBy;
}
