package diplom_project.chinilka.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "products", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class Product extends GenericModel {
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "device_type_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_DEVICE_MODEL_PRODUCT"))
    private DeviceType deviceType;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "image_path", nullable = false)
    private String imagePath;
}
