package diplom_project.chinilka.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "order_lines", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class OrderLine extends GenericModel {
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDER_ORDER_LINE"))
    private Order order;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDER_PRODUCT"))
    private Product product;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;
}
