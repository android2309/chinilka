package diplom_project.chinilka.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "categories", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class Category extends GenericModel {
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    @JoinTable(name = "category_device_type",
            joinColumns = @JoinColumn(name = "category_id"), foreignKey = @ForeignKey(name = "FK_CATEGORY_DEVICE_TYPES"),
            inverseJoinColumns = @JoinColumn(name = "device_type_id"), inverseForeignKey = @ForeignKey(name = "FK_DEVICE_TYPES_CATEGORY")
    )
    private List<DeviceType> deviceTypes;
}
