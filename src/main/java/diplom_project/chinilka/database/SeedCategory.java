package diplom_project.chinilka.database;

import diplom_project.chinilka.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SeedCategory implements ISeedData{
    @Autowired
    private final CategoryService categoryService;
    @Override
    public void EnsurePopulated() {

    }
}
