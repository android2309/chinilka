package diplom_project.chinilka.repository;

import diplom_project.chinilka.models.Category;
import org.springframework.stereotype.Repository;

@Repository
public interface ICategoryRepository extends IGenericRepository<Category> {
}
