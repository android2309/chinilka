package diplom_project.chinilka.repository;

import diplom_project.chinilka.models.User;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends IGenericRepository<User> {
}
