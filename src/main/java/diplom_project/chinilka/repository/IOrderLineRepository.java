package diplom_project.chinilka.repository;

import diplom_project.chinilka.models.OrderLine;
import org.springframework.stereotype.Repository;

@Repository
public interface IOrderLineRepository extends IGenericRepository<OrderLine> {
}
