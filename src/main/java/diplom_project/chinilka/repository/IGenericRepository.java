package diplom_project.chinilka.repository;

import diplom_project.chinilka.models.GenericModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IGenericRepository<T extends GenericModel> extends JpaRepository<T, Long> {
}
