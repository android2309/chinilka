package diplom_project.chinilka.repository;

import diplom_project.chinilka.models.DeviceType;
import org.springframework.stereotype.Repository;

@Repository
public interface IDeviceTypeRepository extends IGenericRepository<DeviceType> {
}
