package diplom_project.chinilka.repository;

import diplom_project.chinilka.models.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRepository extends IGenericRepository<Role> {
}
