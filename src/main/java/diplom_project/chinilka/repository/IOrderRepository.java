package diplom_project.chinilka.repository;

import diplom_project.chinilka.models.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface IOrderRepository extends IGenericRepository<Order> {
}
