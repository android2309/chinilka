package diplom_project.chinilka.repository;

import diplom_project.chinilka.models.Product;
import org.springframework.stereotype.Repository;

@Repository
public interface IProductRepository extends IGenericRepository<Product> {
}
