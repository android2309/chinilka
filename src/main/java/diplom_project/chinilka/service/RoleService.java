package diplom_project.chinilka.service;

import diplom_project.chinilka.dto.RoleDto;
import diplom_project.chinilka.mapper.RoleMapper;
import diplom_project.chinilka.models.Role;
import diplom_project.chinilka.repository.IRoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService extends GenericService<Role, RoleDto>{
    public RoleService(IRoleRepository repository, RoleMapper mapper) {
        super(repository, mapper);
    }
}
