package diplom_project.chinilka.service;

import diplom_project.chinilka.dto.DeviceTypeDto;
import diplom_project.chinilka.mapper.DeviceTypeMapper;
import diplom_project.chinilka.mapper.GenericMapper;
import diplom_project.chinilka.models.DeviceType;
import diplom_project.chinilka.repository.IDeviceTypeRepository;
import diplom_project.chinilka.repository.IGenericRepository;
import org.springframework.stereotype.Service;

@Service
public class DeviceTypeService extends GenericService<DeviceType, DeviceTypeDto> {
    public DeviceTypeService(IDeviceTypeRepository repository, DeviceTypeMapper mapper) {
        super(repository, mapper);
    }
}
