package diplom_project.chinilka.service;

import diplom_project.chinilka.dto.OrderDto;
import diplom_project.chinilka.mapper.GenericMapper;
import diplom_project.chinilka.mapper.OrderMapper;
import diplom_project.chinilka.models.Order;
import diplom_project.chinilka.repository.IGenericRepository;
import diplom_project.chinilka.repository.IOrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends GenericService<Order, OrderDto>{
    public OrderService(IOrderRepository repository, OrderMapper mapper) {
        super(repository, mapper);
    }
}
