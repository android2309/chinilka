package diplom_project.chinilka.service;

import diplom_project.chinilka.dto.ProductDto;
import diplom_project.chinilka.mapper.ProductMapper;
import diplom_project.chinilka.models.Product;
import diplom_project.chinilka.repository.IProductRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductService extends GenericService<Product, ProductDto>{
    public ProductService(IProductRepository repository, ProductMapper mapper) {
        super(repository, mapper);
    }
}
