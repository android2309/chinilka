package diplom_project.chinilka.service;

import diplom_project.chinilka.dto.CategoryDto;
import diplom_project.chinilka.mapper.CategoryMapper;
import diplom_project.chinilka.models.Category;
import diplom_project.chinilka.repository.ICategoryRepository;
import org.springframework.stereotype.Service;

@Service
public class CategoryService extends GenericService<Category, CategoryDto> {
    public CategoryService(ICategoryRepository repository, CategoryMapper mapper) {
        super(repository, mapper);
    }
}
