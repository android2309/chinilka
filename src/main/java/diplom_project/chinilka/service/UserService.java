package diplom_project.chinilka.service;

import diplom_project.chinilka.dto.UserDto;
import diplom_project.chinilka.mapper.UserMapper;
import diplom_project.chinilka.models.User;
import diplom_project.chinilka.repository.IUserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<User, UserDto>{
    public UserService(IUserRepository repository, UserMapper mapper) {
        super(repository, mapper);
    }
}
