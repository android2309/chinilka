package diplom_project.chinilka.service;

import diplom_project.chinilka.dto.OrderLineDto;
import diplom_project.chinilka.mapper.OrderLineMapper;
import diplom_project.chinilka.models.OrderLine;
import diplom_project.chinilka.repository.IOrderLineRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderLineService extends GenericService<OrderLine, OrderLineDto>{
    public OrderLineService(IOrderLineRepository repository, OrderLineMapper mapper) {
        super(repository, mapper);
    }
}
