package diplom_project.chinilka.service;

import diplom_project.chinilka.dto.GenericDto;
import diplom_project.chinilka.mapper.GenericMapper;
import diplom_project.chinilka.models.GenericModel;
import diplom_project.chinilka.repository.IGenericRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public abstract class GenericService <T extends GenericModel, U extends GenericDto>{
    protected final IGenericRepository<T> repository;
    protected final GenericMapper<T, U> mapper;

    public GenericService(IGenericRepository<T> repository, GenericMapper<T, U> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<U> listAll() {
        return mapper.toDtos(repository.findAll());
    }

    public U getById(final Long id) throws NotFoundException {
        return mapper.toDto(repository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Данных не найдено по заданному id")
                ));
    }

    public U create(U newEntity) {
        newEntity.setCreatedBy("ADMIN");
        newEntity.setCreatedWhen(LocalDateTime.now());
        T entity = mapper.toEntity(newEntity);
        return mapper.toDto(repository.save(entity));
    }

    public U update(U updatedEntity, final Long id) {
        updatedEntity.setUpdatedBy("ADMIN");
        updatedEntity.setUpdatedWhen(LocalDateTime.now());
        updatedEntity.setId(id);
        return mapper.toDto(repository.save(mapper.toEntity(updatedEntity)));
    }

    public void delete(final Long id) {
        repository.deleteById(id);
    }
}
